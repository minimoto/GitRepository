package com.erbadagang.springboot.rsa.util;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.codec.binary.Base64;

import javax.crypto.*;
import javax.crypto.spec.SecretKeySpec;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

/**
 * @description AES对称加解密算法，AES加密解密工具类
 * @ClassName: AesUtils
 * @author: 郭秀志 jbcode@126.com
 * @date: 2020/7/25 11:37
 * @Copyright:
 */
@Slf4j
public class AesUtils {

    private AesUtils() {

    }

    /**
     * 生成Aes的密钥key。keysize: must be equal to 128, 192 or 256
     * 另外一种方法：String AesKey =RandomStringUtils.randomAlphanumeric(16);
     *
     * @param length
     * @return
     * @throws Exception
     */
    public static byte[] generateAesKey(int length) throws Exception {
        //实例化
        KeyGenerator kgen = null;
        kgen = KeyGenerator.getInstance("AES");
        //设置密钥长度
        kgen.init(length);
        //生成密钥
        SecretKey skey = kgen.generateKey();
        //返回密钥的二进制编码
        return skey.getEncoded();
    }

    /**
     * AES 对称 加密 Data, 产生 encryptData
     *
     * @param data 需要加密的内容
     * @param key  加密密码 我们自己设置的。
     */
    public static final String encrypt(final String data, final String key)
            throws InvalidKeyException, BadPaddingException, IllegalBlockSizeException, NoSuchAlgorithmException, NoSuchPaddingException {
        Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
        SecretKeySpec keySpec = new SecretKeySpec(key.getBytes(), "AES");
        // 3 初始化Cipher实例。设置执行模式以及加密密钥
        cipher.init(Cipher.ENCRYPT_MODE, keySpec);
        // 4 执行
        return Base64.encodeBase64String(cipher.doFinal(data.getBytes()));
    }

    /**
     * AES 对称 加密 Data, 产生 encryptData
     *
     * @param data 需要加密的内容
     * @param key  加密密码 我们自己设置的。
     */
    public static final String encrypt(final String data, final byte[] key)
            throws InvalidKeyException, BadPaddingException, IllegalBlockSizeException, NoSuchAlgorithmException, NoSuchPaddingException {
        Cipher cipher = Cipher.getInstance("AES/ECB/PKCS5Padding");
        SecretKeySpec keySpec = new SecretKeySpec(key, "AES");
        // 3 初始化Cipher实例。设置执行模式以及加密密钥
        cipher.init(Cipher.ENCRYPT_MODE, keySpec);
        // 4 执行
        return Base64.encodeBase64String(cipher.doFinal(data.getBytes()));
    }

    /**
     * AES 对称解密
     *
     * @param data 待解密内容
     * @param key  解密密钥
     */
    public static final byte[] decrypt(final String data, final byte[] key)
            throws InvalidKeyException, BadPaddingException, IllegalBlockSizeException, NoSuchPaddingException, NoSuchAlgorithmException {
        Cipher aesCipher = Cipher.getInstance("AES");
        SecretKeySpec keySpec = new SecretKeySpec(key, "AES");
        // 3 初始化Cipher实例。设置执行模式以及加密密钥
        aesCipher.init(Cipher.DECRYPT_MODE, keySpec);
        return aesCipher.doFinal(Base64.decodeBase64(data));
    }

}
