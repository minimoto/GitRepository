package com.erbadagang.springboot.springwebflux.service;

import com.erbadagang.springboot.springwebflux.vo.UserVO;
import org.springframework.stereotype.Service;

@Service
public class UserService {

    public UserVO get(Integer id) {
        return new UserVO().setId(id).setUsername("test");
    }

}
