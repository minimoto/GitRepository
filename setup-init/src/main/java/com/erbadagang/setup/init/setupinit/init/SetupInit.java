package com.erbadagang.setup.init.setupinit.init;

import com.erbadagang.setup.init.setupinit.service.MyService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.annotation.Bean;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

/**
 * StartUpInit作用是：演示如下几种初始化方法
 *
 * @ClassName: StartUpInit
 * @author: 郭秀志 jbcode@126.com
 * @date: 2020/6/26 17:14
 * @Copyright:
 */
@Slf4j
@Component
public class SetupInit {

    @Autowired
    private MyService service;

    @PostConstruct
    public void init() {
        log.info(">>>>>分割线>>>>>");
        log.info(" @PostConstruct 初始化开始");
        log.info(service.getName() + " from PostConstruct");
    }

    /**
     * ApplicationReadyEvent 事件监听当ApplicationReady后执行。
     *
     * @param event
     */
    @EventListener
    public void handleApplicationReady(ApplicationReadyEvent event) {
        log.info(">>>>>分割线>>>>>");
        log.info("The application is ready, 初始化开始");
        log.info(service.getName() + " from ApplicationReadyEvent");
    }

    @Bean(initMethod = "init")
    public InitMethodBean initMethodBean() {
        return new InitMethodBean();
    }

    class InitMethodBean {
        @Autowired
        private MyService service;

        void init() {
            log.info(">>>>>分割线>>>>>");
            log.info(service.getName() + " from InitMethodBean");
        }

        public String getServiceName() {
            return service.getName() + " from the method 'getServiceName' of autowired Bean";
        }
    }
}