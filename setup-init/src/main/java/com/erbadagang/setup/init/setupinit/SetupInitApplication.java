package com.erbadagang.setup.init.setupinit;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SetupInitApplication {

    public static void main(String[] args) {
        SpringApplication.run(SetupInitApplication.class, args);
    }

}
