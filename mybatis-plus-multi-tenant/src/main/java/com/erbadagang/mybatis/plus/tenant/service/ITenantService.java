package com.erbadagang.mybatis.plus.tenant.service;

import com.erbadagang.mybatis.plus.tenant.entity.Tenant;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 郭秀志 jbcode@126.com
 * @since 2020-07-12
 */
public interface ITenantService extends IService<Tenant> {

}
