package com.erbadagang.mybatis.plus.tenant;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MultiTanentApplication {

    public static void main(String[] args) {
        SpringApplication.run(MultiTanentApplication.class, args);
    }

}
