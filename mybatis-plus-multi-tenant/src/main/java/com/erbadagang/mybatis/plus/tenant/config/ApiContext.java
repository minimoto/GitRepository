package com.erbadagang.mybatis.plus.tenant.config;

import org.springframework.stereotype.Component;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @description 系统的上下文帮助类。ConcurrentHashMap设置租户ID，供后续的MP的getTenantId()取出
 * @ClassName: ApiContext
 * @author: 郭秀志 jbcode@126.com
 * @date: 2020/7/12 21:50
 * @Copyright:
 */
@Component
public
class ApiContext {
    private static final String KEY_CURRENT_TENANT_ID = "KEY_CURRENT_TENANT_ID";
    private static final Map<String, Object> mContext = new ConcurrentHashMap<>();

    public void setCurrentTenantId(Long providerId) {
        mContext.put(KEY_CURRENT_TENANT_ID, providerId);
    }

    public Long getCurrentTenantId() {
        return (Long) mContext.get(KEY_CURRENT_TENANT_ID);
    }
}