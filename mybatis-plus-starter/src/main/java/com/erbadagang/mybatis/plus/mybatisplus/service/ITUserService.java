package com.erbadagang.mybatis.plus.mybatisplus.service;

import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author 郭秀志 jbcode@126.com
 * @since 2020-07-10
 */
public interface ITUserService<TUser> extends IService<TUser> {

}
