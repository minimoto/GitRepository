package com.erbadagang.mybatis.plus.mybatisplus.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.erbadagang.mybatis.plus.mybatisplus.entity.TUser;

/**
 * <p>
 * Mapper 接口
 * </p>
 *
 * @author 郭秀志 jbcode@126.com
 * @since 2020-07-10
 */
public interface TUserMapper extends BaseMapper<TUser> {

}
