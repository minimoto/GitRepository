package com.erbadagang.mybatis.plus.mybatisplus.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.erbadagang.mybatis.plus.mybatisplus.entity.User;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author 郭秀志 jbcode@126.com
 * @since 2020-07-11
 */
public interface IUserService extends IService<User> {

}
