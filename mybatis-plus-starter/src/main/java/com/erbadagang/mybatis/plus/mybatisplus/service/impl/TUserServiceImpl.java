package com.erbadagang.mybatis.plus.mybatisplus.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.erbadagang.mybatis.plus.mybatisplus.entity.TUser;
import com.erbadagang.mybatis.plus.mybatisplus.mapper.TUserMapper;
import com.erbadagang.mybatis.plus.mybatisplus.service.ITUserService;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 服务实现类
 * </p>
 *
 * @author 郭秀志 jbcode@126.com
 * @since 2020-07-10
 */
@Service
public class TUserServiceImpl extends ServiceImpl<TUserMapper, TUser> implements ITUserService<TUser> {

}
