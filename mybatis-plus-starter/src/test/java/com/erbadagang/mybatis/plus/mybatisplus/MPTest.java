package com.erbadagang.mybatis.plus.mybatisplus;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.erbadagang.mybatis.plus.mybatisplus.entity.TUser;
import com.erbadagang.mybatis.plus.mybatisplus.entity.User;
import com.erbadagang.mybatis.plus.mybatisplus.mapper.UserMapper;
import com.erbadagang.mybatis.plus.mybatisplus.service.ITUserService;
import com.erbadagang.mybatis.plus.mybatisplus.service.IUserService;
import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 * @description MP简单测试。
 * @ClassName: MPTest
 * @author: 郭秀志 jbcode@126.com
 * @date: 2020/7/9 14:53
 * @Copyright:
 */
//@RunWith(SpringRunner.class)
@SpringBootTest
public class MPTest {

    @Autowired
    private UserMapper userMapper;

    @Autowired
    private ITUserService<TUser> tUserService;


    @Test
    public void testSelect() {
        System.out.println(("----- selectAll method test ------"));
        List<User> userList = userMapper.selectList(null);
        Assert.assertEquals(5, userList.size());
        userList.forEach(System.out::println);

        System.out.println(("----- select via ids test ------"));
        userList = userMapper.selectBatchIds(Arrays.asList(1, 5));
        userList.forEach(System.out::println);
    }

    @Test
    public void testQueryWrapper() {
        // Step1：创建一个 QueryWrapper 对象
        QueryWrapper<User> queryWrapper = new QueryWrapper<>();

        // Step2： 构造查询条件
        queryWrapper
                .select("id", "name", "age")
                .lt("age", 23)
                .like("name", "i");

        // Step3：执行查询
        userMapper
                .selectList(queryWrapper)
                .forEach(System.out::println);
    }

    @Test
    public void selectPage() {
        // 根据Wrapper 自定义条件查询
        QueryWrapper<User> queryWrapper = new QueryWrapper<>();
        queryWrapper.gt("age", "18");
        queryWrapper.orderByDesc("age");
        Page<User> userPage = new Page<User>(2, 2);
        // userPage.setCurrent(2L);  //当前是第几页 默认为1
        // userPage.setSize(2);  //每页大小
        IPage<User> userIPage = userMapper.selectPage(userPage, queryWrapper);

        System.out.println("当前页" + userIPage.getCurrent());  //当前页
        System.out.println("总页数" + userIPage.getPages()); //总页数
        System.out.println("返回数据" + userIPage.getRecords());  //返回数据
        System.out.println("每页大小" + userIPage.getSize());  //每页大小
        System.out.println("满足符合条件的条数" + userIPage.getTotal());  //满足符合条件的条数
        System.out.println("下一页" + userPage.hasNext());   //下一页
        System.out.println("上一页" + userPage.hasPrevious());  //上一页
    }

    @Test
    public void selectMaps() {
        Page<User> page = new Page<User>(1, 5);
        QueryWrapper<User> queryWrapper = new QueryWrapper<>();

        List<Map<String, Object>> maps = userMapper.selectMaps(queryWrapper);
        maps.forEach(map -> {
            System.out.println("name-->" + map.get("name"));
            System.out.println("email-->" + map.get("email"));
        });
        System.out.println(maps);
    }

    @Test
    public void testUpdateById() {
        User user = userMapper.selectById(4);
        user.setAge(88);
        userMapper.updateById(user);
    }

    @Test
    public void testUpdateWrapper() {
        int reduceAge = 2;
        User user = new User();

        // 更新用户数据的wrapper
        UpdateWrapper<User> updateWrapper = new UpdateWrapper<User>();
        //修改数据的语句
        updateWrapper.set("email", "winspace@erbadagang.com");//使email字段的值更新为“winspace@erbadagang.com”
        updateWrapper.setSql("age = age - " + reduceAge);//自定义的sql语句
        //条件
        updateWrapper.eq("id", 4);
        userMapper.update(user, updateWrapper);
    }

    /**
     * <p>
     * 根据根据 entity 条件，删除记录,QueryWrapper实体对象封装操作类（可以为 null）
     * 下方获取到queryWrapper后删除的查询条件为name字段为null的and年龄大于等于12的and email字段不为null的
     * 同理写法条件添加的方式就不做过多介绍了。
     * </p>
     */
    @Test
    public void delete() {
        QueryWrapper<User> queryWrapper = new QueryWrapper<>();
        queryWrapper
                .isNull("name")
                .ge("age", 12)
                .isNotNull("email");
        int delete = userMapper.delete(queryWrapper);
        System.out.println("delete return count = " + delete);
    }

    /**
     * <p>
     * 根据 entity 条件，查询一条记录,
     * 这里和上方删除构造条件一样，只是seletOne返回的是一条实体记录，当出现多条时会报错
     * </p>
     */
    @Test
    public void selectOne() {
        QueryWrapper<User> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("name", "guo");

        User user = userMapper.selectOne(queryWrapper);
        System.out.println(user);
    }

    /**
     * <p>
     * 根据 Wrapper 条件，查询总记录数
     * </p>
     */
    @Test
    public void selectCount() {
        QueryWrapper<User> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("name", "guo");

        Integer count = userMapper.selectCount(queryWrapper);
        System.out.println(count);
    }

    /**
     * <p>
     * 根据 entity 条件，查询全部记录
     * </p>
     */
    @Test
    public void selectListByEntity() {
        List<User> list = userMapper.selectList(null);//null为无条件

        System.out.println(list);
    }

    /**
     * <p>
     * 根据 Wrapper 条件，查询全部记录
     * </p>
     */
    @Test
    public void selectListByMapper() {
        QueryWrapper<User> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("name", "guo");

        List<User> list = userMapper.selectList(queryWrapper);//null为无条件

        System.out.println(list);
    }

    @Test
    public void testService() {
        TUser user = new TUser();
        user.setUserName("trek");
        user.setPassword("888999");
        user.setPwdCipher("ewifwiEFafe==");
        if (tUserService.save(user)) {
            tUserService.list().forEach(System.out::println);
        } else {
            System.out.println("添加数据失败");
        }
    }

    /**
     * 测试插入的自动填充数据功能。
     */
    @Test
    public void testAutoFillInsert() {
        User user = new User();
        user.setId(0l);
        user.setName("崔克");
        user.setAge(18);
        user.setEmail("trek@erbadagang.cn");

        int id = userMapper.insert(user);//自动返回插入的id

        System.out.println(id);
    }

    /**
     * 测试更新的自动填充数据功能。
     */
    @Test
    public void testAutoFillUpdate() {
        User user = new User();
        user.setId(7l);
        user.setName("trek");
        user.setAge(28);
        user.setEmail("trek@erbadagang.cn");

        int id = userMapper.updateById(user);//自动返回插入的id

        System.out.println(id);
    }


    @Autowired
    private IUserService userService;

    /**
     * 逻辑删除测试。
     */
    @Test

    public void testDelete() {
        if (userService.removeById(8)) {
            System.out.println("删除数据成功");
            userService.list().forEach(System.out::println);
        } else {
            System.out.println("删除数据失败");
        }
    }

    /**
     * 乐观锁测试
     */
    @Test
    public void testVersion() {
        User user = new User();
        user.setName("Look");
        user.setAge(8);
        user.setEmail("look@erbadagang.cn");
        userService.save(user);//新增数据
        userService.list().forEach(System.out::println);//查询数据

        user.setName("梅花");
        userService.update(user, null);//修改数据
        userService.list().forEach(System.out::println);//查询数据
    }
}