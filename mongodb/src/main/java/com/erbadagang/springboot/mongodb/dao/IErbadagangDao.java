package com.erbadagang.springboot.mongodb.dao;

import com.erbadagang.springboot.mongodb.entity.ErbadagangEntity;

/**
 * @description 操作数据库的接口。
 * @ClassName: IErbadagangDao
 * @author: 郭秀志 jbcode@126.com
 * @date: 2020/8/8 17:02
 * @Copyright:
 */
public interface IErbadagangDao {

    void saveDemo(ErbadagangEntity erbadagangEntity);

    void removeDemo(Long id);

    void updateDemo(ErbadagangEntity erbadagangEntity);

    ErbadagangEntity findDemoById(Long id);
}