package com.erbadagang.data.jpa.service;

import com.erbadagang.data.jpa.entity.UserEntity;
import java.util.Optional;
/**
 * (User)表服务接口
 *
 * @author 郭秀志 jbcode@126.com
 * @since 2020-07-13 11:13:03
 */
public interface IUserService {
    void save(UserEntity userEntity);
    
    Optional<UserEntity> findById(Long id);
}