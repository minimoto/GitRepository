package com.erbadagang.data.jpa.entity;

import com.erbadagang.data.jpa.base.BaseEntry;
import lombok.Data;
import lombok.experimental.Accessors;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * @description 继承自BaseEntry，很多公用属性不用定义，会自动生成表结构。
 * @ClassName: UserBaseEntity
 * @author: 郭秀志 jbcode@126.com
 * @date: 2020/7/14 17:56
 * @Copyright:
 */
@Data
@Entity
@Table(name = "t_user_base")
//User类的@Accessors(chain = true)注解代表可以链式写法来赋值。
@Accessors(chain = true)
public class UserBaseEntity extends BaseEntry {

    /**
     * 账号
     */
    @Column(nullable = false)
    private String username;
    /**
     * 密码（明文）
     * <p>
     * ps：生产环境下，千万不要明文噢
     */
    @Column(nullable = false)
    private String password;

}