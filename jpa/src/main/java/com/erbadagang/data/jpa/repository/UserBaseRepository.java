package com.erbadagang.data.jpa.repository;

import com.erbadagang.data.jpa.base.BaseRepostitory;
import com.erbadagang.data.jpa.entity.UserBaseEntity;

/**
 * @description 继承自定义的BaseRepostitory。
 * @ClassName: UserBaseRepository
 * @author: 郭秀志 jbcode@126.com
 * @date: 2020/7/14 19:09
 * @Copyright:
 */
public interface UserBaseRepository extends BaseRepostitory<UserBaseEntity, Integer> {

}