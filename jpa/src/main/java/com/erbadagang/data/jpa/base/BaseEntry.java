package com.erbadagang.data.jpa.base;

import lombok.Data;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * @description Entry的积累，所有的entry都可以继承它。包括了常规的公共字段。
 * @ClassName: BaseEntry
 * @author: 郭秀志 jbcode@126.com
 * @date: 2020/7/14 17:11
 * @Copyright:
 */
@Data
@MappedSuperclass //这个注解表示在父类上面的，用来标识父类。
public class BaseEntry implements Serializable {

    private static final long serialVersionUID = 1521226766659220492L;

    //自增主键
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY,  // strategy 设置使用数据库主键自增策略；
            generator = "JDBC") // generator 设置插入完成后，查询最后生成的 ID 填充到该属性中。
    protected Integer id;

    //创建时间
    @Temporal(TemporalType.TIMESTAMP)
    protected Date createdDate;

    //创建人
    protected String createdBy;

    //更新时间
    @Temporal(TemporalType.TIMESTAMP)
    protected Date updatedDate;

    //修改人
    protected String updatedBy;

    //逻辑删除（0 未删除、1 删除）
    protected Integer deleted = 0;

    // 版本号（用于乐观锁， 默认为 1）
    @Basic
    @Column(name = "version")
    protected Integer version;
}