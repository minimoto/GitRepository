package com.erbadagang.data.jpa.controller;

import com.erbadagang.data.jpa.entity.UserEntity;
import com.erbadagang.data.jpa.service.IUserService;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.Optional;

/**
 * (User)表控制层
 *
 * @author 郭秀志 jbcode@126.com
 * @since 2020-07-13 11:13:05
 */
@RestController
@RequestMapping("user")
public class UserController {
    /**
     * 服务对象
     */
    @Resource
    private IUserService userService;

    /**
     * 通过主键查询单条数据
     *
     * @param id 主键
     * @return 单条数据
     */
    @GetMapping("selectOne")
    public Optional<UserEntity> findById(Long id) {
        return this.userService.findById(id);
    }

}