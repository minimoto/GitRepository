package com.erbadagang.data.jpa.entity;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * (User)实体类
 *
 * @author 郭秀志 jbcode@126.com
 * @since 2020-07-13 11:12:53
 */

@Data
@NoArgsConstructor
@Entity
@Table(name = "user", schema = BankConstants.SCHEMA)
public class UserEntity implements Serializable {

    private static final long serialVersionUID = 610190591497872796L;

    /**
     * 主键ID
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    /**
     * 姓名
     */
    @Basic
    @Column(name = "name")
    private String name;

    /**
     * 年龄
     */
    @Basic
    @Column(name = "age")
    private Integer age;

    /**
     * 邮箱
     */
    @Basic
    @Column(name = "email")
    private String email;

    /**
     * 创建时间
     */
    @Basic
    @Column(name = "create_time")
    private Date createTime;

    /**
     * 修改时间
     */
    @Basic
    @Column(name = "update_time")
    private Date updateTime;

    /**
     * 操作人
     */
    @Basic
    @Column(name = "operator")
    private String operator;

    /**
     * 逻辑删除（0 未删除、1 删除）
     */
    @Basic
    @Column(name = "delete_flag")
    private Integer deleteFlag;

    /**
     * 版本号（用于乐观锁， 默认为 1）
     */
    @Basic
    @Column(name = "version")
    private Integer version;

}