package com.erbadagang.data.jpa;

import com.erbadagang.data.jpa.entity.UserBaseEntity;
import com.erbadagang.data.jpa.repository.UserBaseRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.util.Assert;

import java.io.Serializable;
import java.util.Date;
import java.util.Optional;
import java.util.UUID;

/**
 * @description 自定义的repository，可以进行逻辑删除
 * @ClassName: UserBaseRepositoryTest
 * @author: 郭秀志 jbcode@126.com
 * @date: 2020/7/14 19:38
 * @Copyright:
 */
@SpringBootTest(classes = JpaApplication.class)
public class UserBaseRepositoryTest {
    @Autowired
    private UserBaseRepository userRepository;

    @Test // 插入一条记录
    public void testSave() {
        UserBaseEntity user = new UserBaseEntity().
                setUsername(UUID.randomUUID().toString()).
                setPassword("闪电");
        user.setCreatedDate(new Date());
        user.setCreatedBy("specialized");
        userRepository.save(user);
    }

    @Test // 更新一条记录
    public void testUpdate() {
        // 先查询一条记录
        Optional<UserBaseEntity> userDO = userRepository.findById(1);
        Assert.isTrue(userDO.isPresent(), "记录不能为空");
        // 更新一条记录
        UserBaseEntity updateUser = userDO.get();
        updateUser.setPassword("updatedPwd");
        userRepository.save(updateUser);
    }

    @Test // 根据 ID 编号，逻辑删除一条记录
    public void testLogicDelete() {
        userRepository.logicDelete(1);
    }

    /**
     * getOne方法是自定义附加逻辑删除条件的查询方法。详见:{@link com.erbadagang.data.jpa.base.BaseRepostitory#getOne(Serializable)}  getOne方法使用了逻辑删除字段}
     * 本测试是根据 ID 编号，查询一条没被逻辑删除记录。
     * 输出的sql包括逻辑删除的条件：
     * Hibernate: select userbaseen0_.id as id1_1_, userbaseen0_.created_by as created_2_1_, userbaseen0_.created_date as created_3_1_, userbaseen0_.deleted as deleted4_1_, userbaseen0_.updated_by as updated_5_1_, userbaseen0_.updated_date as updated_6_1_, userbaseen0_.version as version7_1_, userbaseen0_.password as password8_1_, userbaseen0_.username as username9_1_ from t_user_base userbaseen0_ where userbaseen0_.id=? and userbaseen0_.deleted=0
     */
    @Test
    public void testGetOne() {
        UserBaseEntity userBaseEntity = userRepository.getOne(1);
        System.out.println("userBaseEntity = " + userBaseEntity);
        System.out.println("CreatedBy属性值 = " + userBaseEntity.getCreatedBy());
        System.out.println("Id属性值 = " + userBaseEntity.getId());
    }

}