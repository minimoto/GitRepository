package com.erbadagang.data.jpa;

import com.erbadagang.data.jpa.entity.UserDO;
import com.erbadagang.data.jpa.repository.UserRepository05;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.transaction.annotation.Transactional;


@SpringBootTest
public class UserRepository05Test {

    @Autowired
    private UserRepository05 userRepository;

    @Test
    public void testFindIdByUsername01() {
        UserDO user = userRepository.findByUsername01("35e89c7b-bd4b-45d1-98d0-567db0181b48");
        System.out.println(user);
    }

    @Test
    public void testFindIdByUsername02() {
        UserDO user = userRepository.findByUsername02("35e89c7b-bd4b-45d1-98d0-567db0181b48");
        System.out.println(user);
    }

    @Test
    public void testFindIdByUsername03() {
        UserDO user = userRepository.findByUsername03("35e89c7b-bd4b-45d1-98d0-567db0181b48");
        System.out.println(user);
    }

    @Test
    // 更新操作，需要在事务中。
    // 在单元测试中，事务默认回滚，所以可能怎么测试，事务都不更新。
    @Transactional
    public void testUpdateUsernameById() {
        userRepository.updateUsernameById(5, "郭秀志");
    }

}