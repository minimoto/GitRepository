package com.erbadagang.data.jpa;

import com.erbadagang.data.jpa.entity.UserBaseEntity;
import com.erbadagang.data.jpa.repository.UserSpecificationRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.util.StringUtils;

import javax.persistence.criteria.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;


@SpringBootTest(classes = JpaApplication.class)
public class UserSpecificationRepositoryTest {
    @Autowired
    private UserSpecificationRepository userRepository;

    @Test
    public void testFindOne() {
        //匿名内部类
        /**
         * 自定义查询条件
         *      1.实现Specification接口（提供泛型：查询的对象类型）
         *      2.实现toPredicate方法（构造查询条件）
         *      3.需要借助方法参数中的两个参数（
         *          root：获取需要查询的对象属性
         *          CriteriaBuilder：构造查询条件的，内部封装了很多的查询条件（模糊匹配，精准匹配）
         *       ）
         *
         */
        Specification<UserBaseEntity> spec = new Specification<UserBaseEntity>() {
            @Override
            public Predicate toPredicate(Root<UserBaseEntity> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
                //1.获取比较的属性
                Path<Object> passwordPath = root.get("password");//查询的式属性名，不是表的字段名
                //2.构造查询条件  ：    select * from t_user_base where password = '崔克'
                /**
                 * 第一个参数：需要比较的属性（path对象）
                 * 第二个参数：当前需要比较的取值
                 */
                Predicate predicate = cb.equal(passwordPath, "梅西");//进行精准的匹配  （比较的属性，比较的属性的值）
                return predicate;
            }
        };
        Optional<UserBaseEntity> one = userRepository.findOne(spec);
        System.out.println("one = " + one);
    }

    @Test
    public void testFindOneByMultiConditions() {

        Specification<UserBaseEntity> spec = new Specification<UserBaseEntity>() {
            @Override
            public Predicate toPredicate(Root<UserBaseEntity> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
                Path<Object> createdBy = root.get("createdBy");//创建者，不是字段名是实体类属性
                Path<Object> passwordPath = root.get("password");//查询的式属性名，不是表的字段名

                //构造查询
                //1.构造客户名的精准匹配查询
                Predicate p1 = cb.equal(createdBy, "崔克");//第一个参数，path（属性），第二个参数，属性的取值
                //2..构造所属行业的精准匹配查询
                Predicate p2 = cb.equal(passwordPath, "崔克");
                //3.将多个查询条件组合到一起：组合（满足条件一并且满足条件二：与关系，满足条件一或满足条件二即可：或关系）
                Predicate and = cb.and(p1, p2);//以与的形式拼接多个查询条件
                // cb.or();//以或的形式拼接多个查询条件
                return and;
            }
        };
        Optional<UserBaseEntity> one = userRepository.findOne(spec);
        System.out.println("one = " + one);
    }


    @Test
    public void testFindAll() {
        Specification<UserBaseEntity> spec = new Specification<UserBaseEntity>() {
            @Override
            public Predicate toPredicate(Root<UserBaseEntity> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
                Path<Object> passwordPath = root.get("password");//查询的式属性名，不是表的字段名

                //构造查询
                //1..构造所属行业的模糊匹配查询
                Predicate p = cb.like(passwordPath.as(String.class), "崔%");
                return p;
            }
        };
        List<UserBaseEntity> all = userRepository.findAll(spec);
        all.forEach(System.out::println);
    }


    @Test
    public void testFindAllOrder() {
        Specification<UserBaseEntity> spec = new Specification<UserBaseEntity>() {
            @Override
            public Predicate toPredicate(Root<UserBaseEntity> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
                Path<Object> passwordPath = root.get("password");//查询的式属性名，不是表的字段名

                //构造查询
                //1..构造所属行业的模糊匹配查询
                Predicate p = cb.like(passwordPath.as(String.class), "崔%");
                return p;
            }
        };
        Sort sort = Sort.by(Sort.Direction.DESC, "id");
        List<UserBaseEntity> all = userRepository.findAll(spec, sort);
        all.forEach(System.out::println);
    }

    @Test
    public void testFindAllPageOrder() {
        Specification<UserBaseEntity> spec = new Specification<UserBaseEntity>() {
            @Override
            public Predicate toPredicate(Root<UserBaseEntity> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
                Path<Object> passwordPath = root.get("password");//查询的式属性名，不是表的字段名

                //构造查询
                //1..构造所属行业的模糊匹配查询
                Predicate p = cb.like(passwordPath.as(String.class), "崔%");
                return p;
            }
        };
        Sort sort = Sort.by(Sort.Direction.DESC, "id");
        //PageRequest对象是Pageable接口的实现类
        Pageable pageable = PageRequest.of(0, 2, sort);  // page 从 0 开始 ，2是指每个page的条数，这个意思是id排序分页查询，每次查询2个数据

        Page<UserBaseEntity> all = userRepository.findAll(spec, pageable);
        all.forEach(System.out::println);
    }

    //模拟入参
    String createdBy = null;
    String password = null;

    @Test
    public void testFindAllPageOrderWithDynamicParameter() {
        createdBy = "trek";
        password = null;

        Specification<UserBaseEntity> spec = new Specification<UserBaseEntity>() {
            @Override
            public Predicate toPredicate(Root<UserBaseEntity> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
                //用于暂时存放查询条件的集合
                List<Predicate> predicatesList = new ArrayList<>();

                if (!StringUtils.isEmpty(createdBy)) {
                    //构造查询
                    Path<Object> createdByPath = root.get("createdBy");//创建者，不是字段名是实体类属性
                    //1.构造客户名的精准匹配查询
                    Predicate p1 = cb.equal(createdByPath, createdBy);//第一个参数，path（属性），第二个参数，属性的取值
                    predicatesList.add(p1);
                }
                if (!StringUtils.isEmpty(password)) {
                    //2..构造所属行业的精准匹配查询
                    Path<Object> passwordPath = root.get("password");//查询的式属性名，不是表的字段名
                    Predicate p2 = cb.equal(passwordPath, password);
                    predicatesList.add(p2);
                }
                //3.将多个查询条件组合到一起：组合（满足条件一并且满足条件二：与关系，满足条件一或满足条件二即可：或关系）
                Predicate and = cb.and(predicatesList.toArray(new Predicate[predicatesList.size()]));//以与的形式拼接多个查询条件
                // cb.or();//以或的形式拼接多个查询条件
                return and;
            }
        };
        Sort sort = Sort.by(Sort.Direction.DESC, "id");
        //PageRequest对象是Pageable接口的实现类
        Pageable pageable = PageRequest.of(0, 2, sort);  // page 从 0 开始 ，2是指每个page的条数，这个意思是id排序分页查询，每次查询2个数据

        Page<UserBaseEntity> all = userRepository.findAll(spec, pageable);
        all.forEach(System.out::println);
    }

}