package com.erbadagang.data.jpa;

import com.erbadagang.data.jpa.entity.UserDO;
import com.erbadagang.data.jpa.repository.UserRepository01;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.util.Assert;

import java.util.Arrays;
import java.util.Date;
import java.util.Optional;
import java.util.UUID;

@SpringBootTest(classes = JpaApplication.class)
public class UserRepository01Test {
    @Autowired
    private UserRepository01 userRepository;

    @Test // 插入一条记录
    public void testSave() {
        UserDO user = new UserDO().
                setUsername(UUID.randomUUID().toString()).
                setPassword("guoxiuzhi").
                setCreateTime(new Date());
        userRepository.save(user);
    }

    @Test // 更新一条记录
    public void testUpdate() {
        // 先查询一条记录
        Optional<UserDO> userDO = userRepository.findById(1);
        Assert.isTrue(userDO.isPresent(), "记录不能为空");
        // 更新一条记录
        UserDO updateUser = userDO.get();
        updateUser.setPassword("oliverpwd");
        userRepository.save(updateUser);
    }

    @Test // 根据 ID 编号，删除一条记录
    public void testDelete() {
        userRepository.deleteById(2);
    }

    @Test // 根据 ID 编号，查询一条记录
    public void testSelectById() {
        Optional<UserDO> userDO = userRepository.findById(1);
        System.out.println(userDO.get());
    }

    @Test // 根据 ID 编号数组，查询多条记录
    public void testSelectByIds() {
        Iterable<UserDO> users = userRepository.findAllById(Arrays.asList(1, 5));
        users.forEach(System.out::println);
    }
}