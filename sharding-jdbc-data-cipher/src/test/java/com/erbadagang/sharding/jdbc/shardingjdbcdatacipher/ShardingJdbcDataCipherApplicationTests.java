package com.erbadagang.sharding.jdbc.shardingjdbcdatacipher;

import com.erbadagang.sharding.jdbc.shardingjdbcdatacipher.dao.UserDao;
import com.erbadagang.sharding.jdbc.shardingjdbcdatacipher.entity.User;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

/**
 * @description 数据脱敏测试类，包括insert后数据库密文存储，及加密字段查询功能。
 * @ClassName: ShardingJdbcDataCipherApplicationTests
 * @author: 郭秀志 jbcode@126.com
 * @date: 2020/7/2 16:14
 * @Copyright:
 */
@SpringBootTest
class ShardingJdbcDataCipherApplicationTests {

    @Test
    void contextLoads() {
    }

    @Autowired
    private UserDao userDao;

    @Test
    void add() {
        userDao.addUser("guo", "123456");
    }

    @Test
    public void query() {
        List<User> user = userDao.selectUser("guo", "123456");
        user.forEach(System.out::println);
    }

}
