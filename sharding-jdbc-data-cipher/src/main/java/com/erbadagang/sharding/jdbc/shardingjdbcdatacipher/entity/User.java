package com.erbadagang.sharding.jdbc.shardingjdbcdatacipher.entity;


import lombok.Data;

import java.io.Serializable;


/**
 * @description 用户数据entity
 * @ClassName: User
 * @author: 郭秀志 jbcode@126.com
 * @date: 2020/7/2 15:57
 * @Copyright:
 */
@Data
public class User implements Serializable {

    private Long id;

    private String userName;

    private String password;
}