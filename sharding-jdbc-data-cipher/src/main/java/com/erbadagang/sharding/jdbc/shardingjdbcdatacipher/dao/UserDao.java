package com.erbadagang.sharding.jdbc.shardingjdbcdatacipher.dao;


import com.erbadagang.sharding.jdbc.shardingjdbcdatacipher.entity.User;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @description dao数据接口，mybatis数据库操作类。
 * @ClassName: UserDao
 * @author: 郭秀志 jbcode@126.com
 * @date: 2020/7/2 15:58
 * @Copyright:
 */
@Repository
public interface UserDao {

    @Insert("INSERT INTO t_user (user_name,password) VALUES (#{userName}, #{password})")
    void addUser(@Param("userName") String userName, @Param("password") String password);

    @Select("SELECT id,user_name as userName,password  FROM  t_user WHERE user_name=#{userName} and password= #{password}")
    List<User> selectUser(@Param("userName") String userName, @Param("password") String password);

}