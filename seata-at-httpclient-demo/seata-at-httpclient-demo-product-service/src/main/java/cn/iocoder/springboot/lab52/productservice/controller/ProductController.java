package cn.iocoder.springboot.lab52.productservice.controller;

import cn.iocoder.springboot.lab52.productservice.dto.ProductReduceStockDTO;
import cn.iocoder.springboot.lab52.productservice.service.ProductService;
import io.github.yedaxia.apidocs.Docs;
import io.github.yedaxia.apidocs.DocsConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * @description 商品相关操作控制器
 * @ClassName: ProductController
 * @author: 郭秀志 jbcode@126.com
 * @date: 2020/6/23 20:56
 * @Copyright:
 */
@RestController
@RequestMapping("/product")
public class ProductController {

    private Logger logger = LoggerFactory.getLogger(ProductController.class);

    @Autowired
    private ProductService productService;


    /**
     * 减库存操作
     *
     * @param productReduceStockDTO 商品及库存的DTO，用于减库存。
     * @param guo                   示例参数无意义。
     * @return
     */
    @PostMapping("/reduce-stock")
    public Boolean reduceStock(@RequestBody ProductReduceStockDTO productReduceStockDTO, @RequestParam String guo) {
        logger.info(guo + "[reduceStock] 收到减少库存请求, 商品:{}, 价格:{}", productReduceStockDTO.getProductId(),
                productReduceStockDTO.getAmount());
        try {
            productService.reduceStock(productReduceStockDTO.getProductId(), productReduceStockDTO.getAmount());
            // 正常扣除库存，返回 true
            return true;
        } catch (Exception e) {
            // 失败扣除库存，返回 false
            return false;
        }
    }

    /**
     * 示例生成api文档方法
     *
     * @param productId 年龄
     * @param guo       姓氏
     * @return 商品库存DTO
     */
    @PostMapping("/apiDocDemo")
    public ProductReduceStockDTO apiDocDemo(@RequestParam Long productId, @RequestParam String guo) {
        return new ProductReduceStockDTO().setProductId(productId);
    }

    public static void main(String[] args) {
        DocsConfig config = new DocsConfig();
        config.setProjectPath("D:\\dev\\GitRepository\\seata-at-httpclient-demo\\seata-at-httpclient-demo-product-service"); // 项目根目录
        config.setProjectName("商品服务"); // 项目名称
        config.setApiVersion("V1.0");       // 声明该API的版本
        config.setDocsPath("d:\\Japidoc"); // 生成API 文档所在目录
        config.setAutoGenerate(Boolean.TRUE);  // 配置自动生成
        Docs.buildHtmlDocs(config); // 执行生成文档
    }
}
