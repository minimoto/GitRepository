package com.erbadangang.data.mybatis.mybatisstarter.dao;

import com.erbadangang.data.mybatis.mybatisstarter.entity.User;
import org.apache.ibatis.annotations.*;

import java.util.List;
import java.util.Map;

/**
 * @description 表User的dao操作类。mapper用来编写数据库操作方法及对应的SQL注解。
 * @ClassName: UserMapper
 * @author: 郭秀志 jbcode@126.com
 * @date: 2020/7/8 10:08
 * @Copyright:
 */
//@Repository
public interface UserMapper {

    @Select("SELECT * FROM User WHERE NAME = #{name} ")
    List<User> findByName(@Param("name") String name);

    @Select("SELECT * FROM User WHERE ID = #{id} ")
    User findById(@Param("id") Long id);

    @Results({
            @Result(property = "name", column = "name"),
            @Result(property = "age", column = "age")
    })
    @Select("SELECT name, age FROM User")
    List<User> findAll();

    @Insert("INSERT INTO User(NAME, AGE) VALUES(#{name}, #{age})")
    int insert(@Param("name") String name, @Param("age") Integer age);

    @Insert("INSERT INTO User(NAME, AGE) VALUES(#{name,jdbcType=VARCHAR}, #{age,jdbcType=INTEGER})")
    int insertByMap(Map<String, Object> map);

    @Insert("INSERT INTO User(NAME, AGE) VALUES(#{name}, #{age})")
    int insertByUser(User user);

    @Update("UPDATE User SET age=#{age}, name=#{name} WHERE ID=#{id}")
    void update(User user);

    @Delete("DELETE FROM User WHERE id =#{id}")
    void delete(Long id);
}