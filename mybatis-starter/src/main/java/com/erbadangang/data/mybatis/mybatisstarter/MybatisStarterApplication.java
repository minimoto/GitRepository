package com.erbadangang.data.mybatis.mybatisstarter;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @description 启动类。
 * @ClassName: MybatisStarterApplication
 * @author: 郭秀志 jbcode@126.com
 * @date: 2020/7/8 10:10
 * @Copyright:
 */
@SpringBootApplication
@MapperScan("com.erbadangang.data.mybatis.mybatisstarter.dao")
public class MybatisStarterApplication {

    public static void main(String[] args) {
        SpringApplication.run(MybatisStarterApplication.class, args);
    }

}
