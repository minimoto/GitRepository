package com.erbadangang.data.mybatis.mybatisstarter.service.iml;

import com.erbadangang.data.mybatis.mybatisstarter.dao.UserMapper;
import com.erbadangang.data.mybatis.mybatisstarter.entity.User;
import com.erbadangang.data.mybatis.mybatisstarter.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * UserImpl作用是：
 *
 * @ClassName: UserImpl
 * @author: 郭秀志 jbcode@126.com
 * @date: 2020/7/8 11:23
 * @Copyright:
 */
@Service
public class UserImpl implements UserService {
    @Autowired(required = false)//默认required=true,表示注入的时候bean必须存在，否则注入失败
    private UserMapper userMapper;//IDEA提示不能autowired这个UserMapper，可以忽略这个提示。

    @Override
    public List<User> findByName(String name) {
        return userMapper.findByName(name);
    }

    @Override
    public int insert(String name, Integer age) {
        return userMapper.insert(name, age);
    }
}
