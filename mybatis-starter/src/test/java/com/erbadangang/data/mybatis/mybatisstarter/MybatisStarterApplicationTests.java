package com.erbadangang.data.mybatis.mybatisstarter;

import com.erbadangang.data.mybatis.mybatisstarter.dao.UserMapper;
import com.erbadangang.data.mybatis.mybatisstarter.entity.User;
import com.erbadangang.data.mybatis.mybatisstarter.service.UserService;
import lombok.extern.slf4j.Slf4j;
import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @description 测试mapper和service
 * @ClassName: MybatisStarterApplicationTests
 * @author: 郭秀志 jbcode@126.com
 * @date: 2020/7/8 11:09
 * @Copyright:
 */
@SpringBootTest
//@RunWith(SpringRunner.class)
@Slf4j
class MybatisStarterApplicationTests {
    @Autowired//默认required=true,表示注入的时候bean必须存在，否则注入失败
    private UserMapper userMapper;//IDEA提示不能autowired这个UserMapper,可以在类上加@Repository，可以忽略这个提示。


    @Autowired
    private UserService userService;

    @Test
    @Rollback
    public void testMapperInsert() throws Exception {
        userMapper.insert("郭秀志", 20);
        log.info("成功插入值:郭秀志");
    }

    @Test
    public void testMapperFindByName() throws Exception {
        List<User> u = userMapper.findByName("郭秀志");
        u.forEach(System.out::println);
        Assert.assertEquals(20, u.get(0).getAge().intValue());
    }

    @Test
    public void testServiceFindByName() throws Exception {
        List<User> u = userService.findByName("郭秀志");
        u.forEach(user -> log.info("Service test:" + user));
        Assert.assertEquals(20, u.get(0).getAge().intValue());
    }

    @Test
    @Rollback
    public void testMapperInsertByMapParameter() throws Exception {
        Map<String, Object> map = new HashMap<>();
        map.put("name", "guoxiuzhi");
        map.put("age", 40);
        userMapper.insertByMap(map);

        log.info("map传参成功插入值:" + map.get("name"));
    }

    @Test
    @Rollback
    public void testMapperInsertByEntity() throws Exception {
        User user = new User();
        user.setName("guo");
        user.setAge(42);
        userMapper.insertByUser(user);

        log.info("User对象传参成功插入值:" + user.getName());
    }

    @Test
    @Rollback
    public void testMapperUpdateAndDelete() throws Exception {
        //更新操作
        User user = userMapper.findById(1l);
        user.setName("梅西爱骑车");
        user.setAge(5);
        userMapper.update(user);
        log.info("更新完成");

        //删除
        userMapper.delete(4l);
    }

    @Test
    @Rollback
    public void testUserMapper() throws Exception {
        List<User> userList = userMapper.findAll();
        for (User user : userList) {
            Assert.assertEquals(null, user.getId());
            Assert.assertNotEquals(null, user.getName());
            log.info("id：" + user.getId() + ",name=" + user.getName() + ",age=" + user.getAge());
        }
    }
}
