package com.vue.backend.api.config;

import org.springframework.boot.SpringBootConfiguration;
import org.springframework.format.support.FormattingConversionService;
import org.springframework.web.accept.ContentNegotiationManager;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport;
import org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerMapping;
import org.springframework.web.servlet.resource.ResourceUrlProvider;

@SpringBootConfiguration
public class WebConfig extends WebMvcConfigurationSupport {

    @Override
    public RequestMappingHandlerMapping requestMappingHandlerMapping(
            ContentNegotiationManager contentNegotiationManager, FormattingConversionService conversionService,
            ResourceUrlProvider resourceUrlProvider) {
        return new CustomRequestMappingHandlerMapping();
    }

}