package com.vue.backend.entity;

import lombok.Data;

/**
 * @ClassName: ProgramLanguage
 * @Description:记录编程语言的实体类
 * @author: 郭秀志 jbcode@126.com
 * @date: 2020年4月3日 下午2:50:39
 * @Copyright:
 */
@Data
public class ProgramLanguage {

    // 编程语言名称
    private String laguage_name;
    // 星级，代表欢迎度
    private int star_number;
    // 该编程语言的详细描述
    private String desc;

    /**
     * 构造方法。
     *
     * @param laguage_name
     * @param star_number
     * @param desc
     */
    public ProgramLanguage(String laguage_name, int star_number, String desc) {
        this.laguage_name = laguage_name;
        this.star_number = star_number;
        this.desc = desc;
    }
}
