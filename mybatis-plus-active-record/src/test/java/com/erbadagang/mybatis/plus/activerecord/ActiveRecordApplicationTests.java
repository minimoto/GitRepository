package com.erbadagang.mybatis.plus.activerecord;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.erbadagang.mybatis.plus.activerecord.entity.User;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

/**
 * @description 测试代码。
 * @ClassName: ActiveRecordApplicationTests
 * @author: 郭秀志 jbcode@126.com
 * @date: 2020/7/11 21:55
 * @Copyright:
 */
@SpringBootTest
class ActiveRecordApplicationTests {

    @Test
    public void testArInsert() {
        User user = new User();
        //User类的@Accessors(chain = true)注解代表可以链式写法来赋值。
        user.setName("弗鲁姆").setAge(22);

        boolean result = user.insert();
        System.out.println(result);
    }

    @Test
    public void testArUpdate() {
        User user = new User();
        user.setId(12l);
        user.setName("天空车队");
        boolean result = user.updateById();
        System.out.println(result);
    }

    @Test
    public void testArSelect() {
        User user = new User();
        //1、根据id查询
        //user = user.selectById(1);
        //或者这样用
        //user.setId(1);
        //user = user.selectById();

        //2、查询所有
        //List<User> users = user.selectAll();

        //3、根据条件查询
        //List<User> users = user.selectList(new EntityWrapper<User>().like("name","刘"));

        //4、查询符合条件的总数
        QueryWrapper wrapper = new QueryWrapper();
        wrapper.eq("name", "sky");
        int result = user.selectCount(wrapper);
        System.out.println(result);
    }

    @Test
    public void testArDelete() {
        User user = new User();
        //删除数据库中不存在的数据也是返回true
        //1、根据id删除数据
        //boolean result = user.deleteById(1);
        //或者这样写
        //user.setId(1);
        //boolean result = user.deleteById();

        //2、根据条件删除
        QueryWrapper wrapper = new QueryWrapper();
        wrapper.eq("name", "sky");
        boolean result = user.delete(wrapper);
        System.out.println(result);
    }

    @Test
    public void testArPage() {

        User user = new User();
        Page<User> page =
                user.selectPage(new Page<>(1, 4), null);
        List<User> users = page.getRecords();
        System.out.println(users);
    }
}
