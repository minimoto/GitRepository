package com.erbadagang.springcloud.gateway.authservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @description 启动类
 * @ClassName: AuthServiceApplication
 * @author: 郭秀志 jbcode@126.com
 * @date: 2020/7/31 21:43
 * @Copyright:
 */
@SpringBootApplication
public class AuthServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(AuthServiceApplication.class, args);
    }

}
