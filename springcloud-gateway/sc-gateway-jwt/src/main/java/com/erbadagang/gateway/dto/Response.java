package com.erbadagang.gateway.dto;

import lombok.Data;

/**
 * @description 返回数据包装类。
 * @ClassName: Response
 * @author: 郭秀志 jbcode@126.com
 * @date: 2020/7/31 22:03
 * @Copyright:
 */
@Data
public class Response {
    private int code;
    private String message;
    private String data;

    public Response() {

    }

    public Response(int code, String message) {
        this.code = code;
        this.message = message;
    }

    public Response(int code, String message, String data) {
        this.code = code;
        this.message = message;
        this.data = data;
    }

    //省略getter、setter方法
    //......
}