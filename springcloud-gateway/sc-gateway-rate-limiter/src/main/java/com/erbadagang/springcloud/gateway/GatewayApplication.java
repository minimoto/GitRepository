package com.erbadagang.springcloud.gateway;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @description 启动类
 * @ClassName: GatewayApplication
 * @author: 郭秀志 jbcode@126.com
 * @date: 2020/7/29 15:27
 * @Copyright:
 */
@SpringBootApplication
public class GatewayApplication {

    public static void main(String[] args) {
        SpringApplication.run(GatewayApplication.class, args);
    }

}
