package com.erbadagang.springboot.anti.reptile.antireptile;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AntiReptileApplication {

    public static void main(String[] args) {
        SpringApplication.run(AntiReptileApplication.class, args);
    }

}
