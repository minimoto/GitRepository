package com.erbadagang.springboot.anti.reptile.antireptile.service.impl;

import com.erbadagang.springboot.anti.reptile.antireptile.entity.UsersEntity;
import com.erbadagang.springboot.anti.reptile.antireptile.repository.UsersRepository;
import com.erbadagang.springboot.anti.reptile.antireptile.service.IUsersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

/**
 * (Users)表服务实现类
 *
 * @author 郭秀志 jbcode@126.com
 * @since 2020-07-22 09:35:12
 */
@Service("usersService")
public class UsersServiceImpl implements IUsersService {

    @Autowired
    private UsersRepository usersRepository;

    @Override
    public void save(UsersEntity usersEntity) {
        usersRepository.save(usersEntity);
    }

    @Override
    public Optional<UsersEntity> findById(Integer id) {
        return usersRepository.findById(id);
    }
}