package com.erbadagang.springboot.anti.reptile.antireptile.model;


import com.erbadagang.springboot.anti.reptile.antireptile.entity.UsersEntity;

/**
 * (Users)表数据库model
 *
 * @author 郭秀志 jbcode@126.com
 * @since 2020-07-22 09:35:10
 */
public class Users extends UsersEntity {

}