# 郭秀志的开源GitRepository

#### 介绍
以前总是遇到问题去搜索他人的解决方案，也不能总是索取不付出，要是都不写了也就都没了解决方案，为了回馈社会，把自己做的一些功能也开源出去希望对其他程序员有帮助。项目代码对应的技术说明请看博客： [https://url.cn/DoMYIopw](https://url.cn/DoMYIopw)、[https://t.cn/A6LgMb3H](https://t.cn/A6LgMb3H)。

#### 文件夹说明
1. 本仓库的代码使用了如下技术框架并实践：Spring Cloud、Spring boot 3.4.0.RELEASE、MySQL、Redis、Nacos（注册中心、配置中心）、Seata（分布式事务）、Sentinel（限流、熔断、降级）、入门级Spring Cloud Gateway网关（nacos配置规则、jwt认证、限流、sentinel熔断、重试机制、全局异常处理）开发、Sharding sphere（[读写分离](https://www.jianshu.com/p/3b3f7c6fd288)、[分库分表](https://www.jianshu.com/p/f103aae5)、[数据脱敏](https://www.jianshu.com/p/b0cc1dd4f3ff)）。

1. project1文件夹：名称不规范！基于VUE native的前端代码，可发布为H5、微信及支付宝小程序。[代码说明](https://user.qzone.qq.com/52382868/blog/1585976428)

1. parent文件夹：最小化的Spring boot项目模板，新建项目可依此创建节约时间。包括父的pom定义所有子项目的依赖及版本、common子项目（模块）、api项目（暴露接口同时调用common的公用逻辑）。
附：也可参考我的另外一个类似项目，父项目parent-starter，以及它的common-starter子module，以及依赖common的api-service服务module，由于是新建项目时的模板，重要性较高单独一个git仓库[parent-starter源代码](https://gitee.com/jbcode/parent-starter)。[代码说明]( https://user.qzone.qq.com/52382868/blog/1587194712)

1. dataway文件夹：是一种轻量级的接口开发框架，之前通过在后台管理页面配置sql实现Restful API返回json数据的功能，适合简单功能的应用。[代码说明](https://www.jianshu.com/p/23b32699e9fd)

1. setup-init文件夹：Spring boot项目在启动阶段读取入参及初始化资源的几种方式示例。[代码说明](https://www.jianshu.com/p/df5adffcdce0)

1. vue-springboot文件夹：一个简单的vue小程序的后端服务项目，没有使用数据库，数据的查询依赖内存数据。使用了https证书[代码说明](https://user.qzone.qq.com/52382868/blog/1586267753)，钉钉机器人对系统启动或停止报警[代码说明](https://www.jianshu.com/p/e870b7cdb9ed)

1. Flyway文件夹：Flyway是一款开源的数据库版本管理工具，Flyway可以独立于应用实现管理并跟踪数据库的变更，Flyway根据自己的约定，不需要复杂的配置就可以实现数据的Migrate（迁移）。Migrations可以写成SQL脚本，也可以写在Java代码中，本文为Spring Boot整合Flyway。[代码说明](https://www.jianshu.com/p/961b989d4f12)

1. es-spring-data-jest文件夹：使用spring-data-jest方式跟ES打交道。我们会使用 spring-boot-starter-data-jest 自动化配置 Spring Data Jest 主要配置。同时，编写相应的 Elasticsearch 的 CRUD 操作。[代码说明](https://www.jianshu.com/p/f0515a754a05)

1. mybatis-starter文件夹：Spring Boot 2.x使用MyBatis访问MySQL，mybatis-spring-boot-starter主要有两种解决方案，使用注解解决一切问题。[代码说明](https://www.jianshu.com/p/84280101a99c)

1. mybatis-plus-starter文件夹：MyBatis-Plus（简称 MP）是一个 MyBatis 的增强工具，在 MyBatis 的基础上只做增强不做改变，为简化开发、提高效率而生。[代码说明](https://www.jianshu.com/p/3eb1d9eefa27)

1. mybatis-plus-multi-tenant文件夹：MyBatis-Plus多租户即SaaS系统示例。[代码说明](https://www.jianshu.com/p/742f40eb9937)

1. JPA文件夹：jpa的系列文章，包括CRUD、分页、排序、逻辑删除、Specifications动态查询、Proguard代码混淆Jar包。[代码说明系列文章](https://www.jianshu.com/nb/45533284)

1. JWT文件夹：JWT之后，当用户使用它的认证信息登录系统之后，会返回给用户一个JWT， 用户只需要本地保存该 token（通常使用localStorage，也可以使用cookie）即可。当用户希望访问一个受保护的路由或者资源的时候，通常应该在Authorization头部使用Bearer模式添加JWT Token。[代码说明](https://www.jianshu.com/p/161fbab47a49)

1. Kaptcha文件夹：页面验证码相关。[代码说明](https://www.jianshu.com/p/78e4f1a53f25)

1. Tomcat-websocket文件夹：Websocket示例，实现服务端向客户端实时推送消息，实现消息一对一和一对多发送。[代码说明](https://www.jianshu.com/p/8c03bf4e9ac9)

1. anti-reptile文件夹：SpringBoot的反爬虫示例，通过配置ip和ua规则来限定爬虫，如果命中规则会弹出验证码页面，验证通过后访问正常。[代码说明](https://www.jianshu.com/p/0cbdfdf806c4)

1. async文件夹：异步多线程示例。[代码说明](https://www.jianshu.com/p/7e1cadc2441a)

1. RSA&AES文件夹：非对称加密RSA算法结合AES对称加密算法的实现。[代码说明](https://www.jianshu.com/p/d5db48fe434b)

1. redis-distributed-lock文件夹：redis单机模式，实现分布式锁。[代码说明](https://www.jianshu.com/p/5ee7f9207f30)

1. Springboot-rocketmq文件夹：rocketmq在springboot的集成。[代码说明](https://www.jianshu.com/nb/47274371)

1. Springboot-webflux文件夹：webflux响应式编程示例。[代码说明]()

1. MongoDB文件夹：MongoDB数据库操作。[代码说明]()

1. pay_by_user-Java-URF-8：模拟商户提交支付操作到支付宝。

1. Apache-shiro:Apache Shiro™ 是一个功能强大且易于使用的 Java 安全框架，它可以提供身份验证、授权、加密和会话管理的功能。通过 Shiro 易于理解的 API ，你可以快速、轻松地保护任何应用程序 —— 从最小的移动端应用程序到大型的的 Web 和企业级应用程序。[代码说明](https://www.jianshu.com/nb/47555718)

待续......