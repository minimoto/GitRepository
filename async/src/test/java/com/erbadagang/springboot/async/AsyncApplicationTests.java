package com.erbadagang.springboot.async;

import com.erbadagang.springboot.async.task.AsyncTask;
import com.erbadagang.springboot.async.task.AsyncTaskWithThreadPool;
import com.erbadagang.springboot.async.task.SyncTask;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.concurrent.Future;

@SpringBootTest
class AsyncApplicationTests {

    @Autowired
    private SyncTask task;

    @Autowired
    private AsyncTask asyncTask;

    @Test
    public void testAsyncTask() throws Exception {
        asyncTask.doTaskOne();
        asyncTask.doTaskTwo();
        asyncTask.doTaskThree();
    }

    @Test
    public void testAsyncFuture() throws Exception {

        long start = System.currentTimeMillis();

        Future<String> task1 = asyncTask.doTaskOne();
        Future<String> task2 = asyncTask.doTaskTwo();
        Future<String> task3 = asyncTask.doTaskThree();

        while (true) {
            if (task1.isDone() && task2.isDone() && task3.isDone()) {
                // 三个任务都调用完成，退出循环等待
                break;
            }
            Thread.sleep(1000);
        }

        long end = System.currentTimeMillis();

        System.out.println("任务全部完成，总耗时：" + (end - start) + "毫秒");

    }

    @Test
    void contextLoads() {
    }

    @Test
    public void testSyncTask() throws Exception {
        task.doTaskOne();
        task.doTaskTwo();
        task.doTaskThree();
    }


    @Autowired
    private AsyncTaskWithThreadPool asyncTaskWithThreadPool;

    @Test
    public void test() throws Exception {

        asyncTaskWithThreadPool.doTaskOne();
        asyncTaskWithThreadPool.doTaskTwo();
        asyncTaskWithThreadPool.doTaskThree();

//        Thread.currentThread().join();
    }
}
