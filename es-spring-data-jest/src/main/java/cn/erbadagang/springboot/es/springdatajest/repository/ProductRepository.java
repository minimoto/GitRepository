package cn.erbadagang.springboot.es.springdatajest.repository;

import cn.erbadagang.springboot.es.springdatajest.dataobject.ESProductDO;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * @description JPA操作ES
 * @ClassName: ProductRepository
 * @author: 郭秀志 jbcode@126.com
 * @date: 2020/7/5 15:20
 * @Copyright:
 */
public interface ProductRepository extends ElasticsearchRepository<ESProductDO, Integer> {

}
