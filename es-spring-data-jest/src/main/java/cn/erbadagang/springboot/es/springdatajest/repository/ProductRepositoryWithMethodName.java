package cn.erbadagang.springboot.es.springdatajest.repository;


import cn.erbadagang.springboot.es.springdatajest.dataobject.ESProductDO;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * @description 使用JPA的基于方法名的操作，如： findBy、existsBy、countBy、deleteBy 开头。
 * @ClassName: ProductRepositoryWithMethodName
 * @author: 郭秀志 jbcode@126.com
 * @date: 2020/7/6 15:04
 * @Copyright:
 */
public interface ProductRepositoryWithMethodName extends ElasticsearchRepository<ESProductDO, Integer> {

    ESProductDO findByName(String name);

    Page<ESProductDO> findByNameLike(String name, Pageable pageable);

}