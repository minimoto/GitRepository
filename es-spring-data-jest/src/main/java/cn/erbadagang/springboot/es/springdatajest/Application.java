package cn.erbadagang.springboot.es.springdatajest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.data.elasticsearch.ElasticsearchAutoConfiguration;
import org.springframework.boot.autoconfigure.data.elasticsearch.ElasticsearchDataAutoConfiguration;

/**
 * @description ES Jest客户端项目启动类。需要排除 ElasticsearchAutoConfiguration 和 ElasticsearchDataAutoConfiguration 自动配置类，否则会自动配置 Spring Data Elasticsearch 。
 * @ClassName: Application
 * @author: 郭秀志 jbcode@126.com
 * @date: 2020/7/5 10:27
 * @Copyright: http://www.iocoder.cn/Spring-Boot/Elasticsearch/?self
 */
@SpringBootApplication(exclude = {ElasticsearchAutoConfiguration.class, ElasticsearchDataAutoConfiguration.class})
public class Application {

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }
}
