package cn.erbadagang.springboot.es.springdatajest.repository;


import cn.erbadagang.springboot.es.springdatajest.Application;
import cn.erbadagang.springboot.es.springdatajest.dataobject.ESProductDO;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * @description 自定义NativeSearchQuery测试类
 * @ClassName: ProductRepositoryNativeSearchQueryTest
 * @author: 郭秀志 jbcode@126.com
 * @date: 2020/7/7 11:53
 * @Copyright:
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = Application.class)
public class ProductRepositoryNativeSearchQueryTest {

    @Autowired
    private ProductRepositoryNativeSearchQuery productRepository;

    @Test
    public void testSearch() {
        // 查找分类为 1 + 指定关键字，并且按照 id 升序
        Page<ESProductDO> page = productRepository.search(1, "技术",
                PageRequest.of(0, 5, Sort.Direction.ASC, "id"));
        System.out.println(page.getTotalPages());
        //输出查询结果。
        page.forEach(System.out::println);

        // 查找分类为 1 ，并且按照 id 升序
        page = productRepository.search(1, null,
                PageRequest.of(0, 5, Sort.Direction.ASC, "id"));
        System.out.println(page.getTotalPages());
        //输出查询结果。
        page.forEach(System.out::println);
    }

}