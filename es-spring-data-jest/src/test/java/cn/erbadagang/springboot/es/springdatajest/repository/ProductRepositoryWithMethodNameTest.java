package cn.erbadagang.springboot.es.springdatajest.repository;


import cn.erbadagang.springboot.es.springdatajest.Application;
import cn.erbadagang.springboot.es.springdatajest.dataobject.ESProductDO;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.test.context.junit4.SpringRunner;

/**
 * @description 使用JPA命名规范自定义的查询测试
 * @ClassName: ProductRepositoryWithMethodNameTest
 * @author: 郭秀志 jbcode@126.com
 * @date: 2020/7/6 21:21
 * @Copyright:
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = Application.class)
public class ProductRepositoryWithMethodNameTest {

    @Autowired
    private ProductRepositoryWithMethodName productRepository;

    @Test // 根据名字获得一条记录
    public void testFindByName() {
        ESProductDO product = productRepository.findByName("郭秀志源码：98");
        System.out.println(product);
    }

    @Test // 使用 name 模糊查询，分页返回结果
    public void testFindByNameLike() {
        // 根据情况，是否要制造测试数据
        if (true) {
            testInsert();
        }

        // 创建排序条件
        // Sort sort = new Sort(Sort.Direction.DESC, "id");//废弃的写法，需要使用下面写法
        Sort sort = Sort.by(Sort.Direction.DESC, "id");// ID 倒序
        // 创建分页条件。
        Pageable pageable = PageRequest.of(0, 10, sort);
        // 执行分页操作
        Page<ESProductDO> page = productRepository.findByNameLike("郭秀志", pageable);
        // 打印
        System.out.println(page.getTotalElements());
        System.out.println(page.getTotalPages());
        //遍历所有取出ESProductDO数据
        page.get().forEach(System.out::println);
    }

    /**
     * 为了给分页制造一点数据
     */
    private void testInsert() {
        for (int i = 1; i <= 100; i++) {
            ESProductDO product = new ESProductDO();
            product.setId(i); // 一般 ES 的 ID 编号使用 DB 数据对应的编号。先写死成数据循环变化
            product.setName("郭秀志源码：" + i);
            product.setSellPoint("免费开源，不要钱了");
            product.setDescription("描述，description");
            product.setCid(1);
            product.setCategoryName("技术");
            productRepository.save(product);
        }
    }

}