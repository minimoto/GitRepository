package cn.erbadagang.springboot.es.springdatajest.repository;

import cn.erbadagang.springboot.es.springdatajest.Application;
import cn.erbadagang.springboot.es.springdatajest.dataobject.ESProductDO;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Optional;

/**
 * @description
 * @ClassName: ProductRepositoryTest
 * @author: 郭秀志 jbcode@126.com
 * @date: 2020/7/6 9:52
 * @Copyright:
 */
@RunWith(SpringRunner.class)
@SpringBootTest(classes = Application.class)
public class ProductRepositoryTest {

    @Autowired
    private ProductRepository productRepository;

    @Test // 插入一条记录
    public void testInsert() {
        ESProductDO product = new ESProductDO();
        product.setId(1); // 一般 ES 的 ID 编号使用 DB 数据对应的编号。这里先写死
        product.setName("简单JPA操作的ES");
        product.setSellPoint("简单，跟普通JPA data的语法一致进行ES操作");
        product.setDescription("描述：自带CRUD");
        product.setCid(1);
        product.setCategoryName("技术分类");
        productRepository.save(product);

        product.setId(5); // 一般 ES 的 ID 编号使用 DB 数据对应的编号。这里先写死
        product.setName("ES5");
        product.setSellPoint("跟普通JPA data的语法一致进行ES操作");
        product.setDescription("描述5：自带CRUD");
        product.setCid(5);
        product.setCategoryName("技术分类");
        productRepository.save(product);
    }

    // 这里要注意，如果使用 save 方法来更新的话，必须是全量字段，否则其它字段会被覆盖。
    // 所以，这里仅仅是作为一个示例。
    @Test // 更新一条记录
    public void testUpdate() {
        ESProductDO product = new ESProductDO();
        product.setId(1);
        product.setCid(2);
        product.setCategoryName("tech-Java");
        productRepository.save(product);
    }

    @Test // 根据 ID 编号，删除一条记录
    public void testDelete() {
        productRepository.deleteById(1);
    }

    @Test // 根据 ID 编号，查询一条记录
    public void testSelectById() {
        Optional<ESProductDO> userDO = productRepository.findById(1);
        System.out.println("testSelectById():" + userDO);
    }

    @Test // 查询所有记录
    public void testAll() {
        Iterable<ESProductDO> users = productRepository.findAll();
        users.forEach(System.out::println);
    }

}
